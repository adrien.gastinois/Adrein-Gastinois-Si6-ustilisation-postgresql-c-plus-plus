// Librairie
#include <iostream>
#include <libpq-fe.h>

using namespace std;

int main()
{
  // Le pointeur :
  PGconn *connexion;
  // Pour établir la connexion vers PostgreSQL :
  connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=a.gastinois user=a.gastinois connect_timeout=10");

  // Etat de la connexion :
  if(PQstatus(connexion) == CONNECTION_OK)
  {
    cout << "Connexion établie avec les parametres suivants : " << endl;
    cout << "utilisateur : "  << endl;
    cout << "mot de passe : " << endl;
    cout << "base de donnee  : " << endl;
    cout << "port TCP : " <<  endl;
    cout << "chiffrement SSL : "  << endl;
    cout << "encodage : " <<  endl;
    cout << "version du protocole : "  << endl;
    cout << "version du serveur : " <<  endl;
    cout << "version de la bibliotheque Libpq du client : "  << endl;
  }
  else
  {
    cout << "Connexion échouée" << endl;
  }

  return 0;
}
